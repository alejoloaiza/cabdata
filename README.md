
# TECHNICAL TEST - Cab Data API

There is no need to Redis or Mysql to make this work, those will be used from the docker images.

This code was tested on Mac and Linux and works fine following the Installation steps, wasn't tested on Windows though.

## Pre-Requisites:
- Go 
- Git (just to clone the repo)      
- Docker and Docker-compose (On MAC they come together on Linux come separately)             
Check using these commands      
`docker --version`      
`docker-compose --version`      

To install Docker on MAC follow this [link](https://docs.docker.com/docker-for-mac/install/)

## Installation:
Clone this repository
`git clone https://gitlab.com/alejoloaiza/cabdata.git`

## Running Mysql and Redis

Execute the command inside cabdata folder:     
`cd env`    
`./start.sh`  
Make sure ./start.sh has permissions for you to execute it.

### Important: This will download and launch 2 docker images, please be patient.

## Run the Server (run this commands on cabdata dir):

`source ./env/env_server.sh`   
`go build -o cabdata`  
`./cabdata`  


API will be listening on port 5000 , try the endpoints:
- http://localhost:5000/trip/{medallion} (GET) to fetch the trips of a medallion filtering by date
- http://localhost:5000/trips (GET) To fetch the trips of one or more medallions
- http://localhost:5000/cache (DELETE) To delete the cache

Leave it running in that terminal window.

## !!!Important!!!:
If your 2 docker images (Mysql, Redis) are up and running (docker ps), but when you hit the API you get an error like this:     
{"error":"dial tcp 172.19.0.3:3306: connect: connection refused"}       
{"error":"driver: bad connection"}      

It means the MySQL server is not accepting conections yet, the reason is the dump.sql is being processed (cab_trip_data dump). Wait for 3 to 4 minutes and try again. (Run: `docker logs mysql` and check when the dump file processing finishes )

The http server will automatically reconnect to both Mysql and Redis if connection fails or disconnects. a reconnection log will look like this:

`019/06/22 12:43:06 127.0.0.1:53727 GET /trip/D7D598CD99978BD012A87A76A7C891B7?date=2013-12-01&nocache=true 500 ERROR: driver: bad connection `     
`2019/06/22 12:46:47 [::1]:53700 GET /trip/D7D598CD99978BD012A87A76A7C891B7?date=2013-12-01&nocache=true 200 80 ms`     

## Run the Command client (IN ANOTHER TERMINAL):

After all  docker images are running, (you can check this by using docker ps command), execute the commands:

`source ./env/env_client.sh`   
`go build -o cabdata`   
`./cabdata help`    

This is an example command to use the endpoint (http://localhost:5000/trip/{medallion})     
`./cabdata fetch D7D598CD99978BD012A87A76A7C891B7 2013-12-01 true`  

This is an example command to use the endpoint (http://localhost:5000/trips)       
`./cabdata fetchmultiple D7D598CD99978BD012A87A76A7C891B7,0C107B532C1207A74F0D8609B9E092FF false`   

This is an example command to use the endpoint (http://localhost:5000/cache )   
`./cabdata clear`   


## Run tests:

If you have Go installed you can run this command :
`go test -v ./...`
