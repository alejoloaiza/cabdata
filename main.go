package main

import (
	"cabdata/cmd"
	"cabdata/core"
	"cabdata/server"
	"log"
	"os"
	"strconv"
)

func main() {
	runas := os.Getenv("RUNAS")
	if runas == "server" {
		dc := core.NewDataClient(cacheConfig(), dbConfig())

		server.Start(dc, serverConfig())
	}
	cmd.Start()

}

func cacheConfig() *core.CacheOptions {

	if os.Getenv("REDIS_SERVER") == "" {
		log.Fatal("Cache config missing: check your environment variables REDIS_SERVER and REDIS_PASSWORD (if required)")
	}
	cacheOpts := &core.CacheOptions{Server: os.Getenv("REDIS_SERVER"), Password: os.Getenv("REDIS_PASSWORD")}
	return cacheOpts
}

func dbConfig() *core.DatabaseOptions {
	if os.Getenv("DB_HOST") == "" || os.Getenv("DB_USER") == "" || os.Getenv("DB_PASSWORD") == "" || os.Getenv("DB_PORT") == "" || os.Getenv("DB_NAME") == "" {
		log.Fatal("API config missing: check your environment variables DB_HOST/DB_USER/DB_PASSWORD/DB_PORT/DB_NAME")
	}
	port, err := strconv.Atoi(os.Getenv("DB_PORT"))
	if err != nil {
		log.Fatalf("invalid port %v", err)
	}

	DBOpts := &core.DatabaseOptions{Host: os.Getenv("DB_HOST"), Name: os.Getenv("DB_NAME"), UserID: os.Getenv("DB_USER"), Port: port, Password: os.Getenv("DB_PASSWORD")}

	return DBOpts
}

func serverConfig() *server.ServerOptions {
	if os.Getenv("SERVER_PORT") == "" {
		log.Fatal("Rest Server config missing: check your environment variables SERVER_PORT")
	}
	return &server.ServerOptions{Address: os.Getenv("SERVER_PORT")}
}
