package core

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis"
)

// Redis cache
func newRedisCache(CacheOpts *CacheOptions) Cache {
	return &RedisCache{Cache: redis.NewClient(&redis.Options{
		Addr:     CacheOpts.Server,
		Password: CacheOpts.Password,
		DB:       0, //  default DB
	})}
}

func CacheKey(key ...interface{}) string {
	var result string
	for _, k := range key {
		result += fmt.Sprintf("%v", k)
	}
	return result
}

func (r *RedisCache) Set(key string, value interface{}) error {
	data, err := json.Marshal(value)
	if err != nil {
		fmt.Println(err)
	}
	_, err = r.Cache.Set(key, data, 0).Result()
	if err != nil {
		fmt.Printf("Error SetCache: %v", err)
		return err
	}
	return nil
}

func (r *RedisCache) Get(key string) ([]byte, error) {
	val, err := r.Cache.Get(key).Bytes()
	if err != nil {
		fmt.Printf("Error GetCache: %v", err)
		return nil, err
	}
	return val, nil
}

func (r *RedisCache) Exists(key string) bool {
	e, err := r.Cache.Exists(key).Result()
	if err == nil && e == 1 {
		return true
	}
	return false
}

func (r *RedisCache) FlushAll() error {
	_, err := r.Cache.FlushAll().Result()
	if err != nil {
		log.Printf("error with flush all on cache: %v", err)
		return err
	}
	return nil
}

// Mock cache
func newMockCache() Cache {
	s, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	return &MockCache{Cache: s}
}

func (m *MockCache) FlushAll() error {
	m.Cache.FlushAll()
	return nil
}
func (m *MockCache) Exists(key string) bool {
	return m.Cache.Exists(key)
}
func (m *MockCache) Set(key string, value interface{}) error {
	newVal, err := json.Marshal(value)
	if err != nil {
		return err
	}
	return m.Cache.Set(key, string(newVal))
}
func (m *MockCache) Get(key string) ([]byte, error) {
	val, err := m.Cache.Get(key)
	return []byte(val), err
}
