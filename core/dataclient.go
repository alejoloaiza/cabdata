package core

import (
	"database/sql"
	"encoding/json"
	"log"
	"sort"

	"github.com/jmoiron/sqlx"
)

//	must receive one or more medallions and return how many trips each medallion has made.

func (dc *DataClient) FetchTrips(opts FetchTripsQuery) (interface{}, bool, error) {
	var err error
	sort.Strings(opts.CabID)
	cacheKey := CacheKey(opts.CabID)
	trips := []CabTrip{}

	if dc.Cache.Exists(cacheKey) && !opts.NoCache {
		// FROM CACHE
		val, err := dc.Cache.Get(cacheKey)
		if err != nil {
			return nil, true, err
		}
		err = json.Unmarshal([]byte(val), &trips)
		if err != nil {
			return nil, true, err
		}
		return trips, true, nil
	}

	// FROM DATABASE
	baseQuery := `
					SELECT 
					medallion, hack_license, vendor_id, rate_code, store_and_fwd_flag, pickup_datetime, dropoff_datetime, passenger_count, trip_time_in_secs, trip_distance
					FROM cab_trip_data
					WHERE medallion IN (?)
					`

	query, args, _ := sqlx.In(baseQuery, opts.CabID)
	query = dc.DB.Rebind(query)

	err = dc.DB.Select(&trips, query, args...)
	if err != nil {
		return nil, false, err
	}
	err = dc.Cache.Set(cacheKey, trips)
	if err != nil {
		log.Printf("Error with Cache Set, requests wont fail %v", err)
	}
	return trips, false, err

}

func (dc *DataClient) FetchCabTrips(opts FetchCabTripsQuery) (interface{}, bool, error) {
	var err error
	cacheKey := CacheKey(opts.CabID, opts.PickDate)
	trips := []CabTrip{}
	//fmt.Println(opts.CabID, opts.PickDate)
	if dc.Cache.Exists(cacheKey) && !opts.NoCache {
		// FROM CACHE
		val, err := dc.Cache.Get(cacheKey)
		if err != nil {
			return nil, true, err
		}
		err = json.Unmarshal([]byte(val), &trips)
		if err != nil {
			return nil, true, err
		}
		return trips, true, nil
	}

	// FROM DATABASE
	baseQuery := `
					SELECT 
					medallion, hack_license, vendor_id, rate_code, store_and_fwd_flag, pickup_datetime, dropoff_datetime, passenger_count, trip_time_in_secs, trip_distance
					FROM cab_trip_data
					WHERE medallion = ? AND DATE(pickup_datetime) = ?
					`
	err = dc.DB.Select(&trips, baseQuery, opts.CabID, opts.PickDate)
	if err != nil {
		//fmt.Printf("Inside FetchCabTrips Select %v", err)
		return nil, false, err
	}
	err = dc.Cache.Set(cacheKey, trips)
	if err != nil {
		log.Printf("Error with Cache Set, requests wont fail %v", err)
	}
	return trips, false, err

}

func (dc *DataClient) DeleteCache() (interface{}, error) {
	dc.Cache.FlushAll()
	return nil, nil
}

func NewDataClient(CacheOpts *CacheOptions, DatabaseOpts *DatabaseOptions) *DataClient {
	return &DataClient{
		Cache: newRedisCache(CacheOpts),
		DB:    newMysqlDatabase(DatabaseOpts),
	}
}
func NewMockDataClient(mockDB *sql.DB) *DataClient {
	return &DataClient{
		Cache: newMockCache(),
		DB:    newMockMysql(mockDB),
	}
}
