package core

import (
	"fmt"
	"log"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
)

// TestMedallionSearchByDate will validate the query logic, number of records and the value retrieved
func TestMedallionSearchByDate(t *testing.T) {
	testMedallionID := "D7D598CD99978BD012A87A76A7C891B7"
	testPickDate := "2013-12-01"
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionID, "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionID, testPickDate).WillReturnRows(rows)

	dc := NewMockDataClient(mockDB)
	opts := FetchCabTripsQuery{
		CabID:    testMedallionID,
		PickDate: testPickDate,
		NoCache:  false,
	}
	resp, _, err := dc.FetchCabTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	trips, ok := resp.([]CabTrip)
	if !ok {
		log.Println("Invalid response")
		t.FailNow()
	}
	if len(trips) != 1 {
		log.Println("Invalid number of trips")
		t.FailNow()
	}
	if trips[0].Medallion != testMedallionID {
		log.Println("Invalid medallion ID for results")
		t.FailNow()
	}
}

// TestMedallionSearchByDateCached will validate that the cache is working, the second fetch should come from the cache.
func TestMedallionSearchByDateCached(t *testing.T) {
	testMedallionID := "D7D598CD99978BD012A87A76A7C891B7"
	testPickDate := "2013-12-01"
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionID, "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionID, testPickDate).WillReturnRows(rows)

	dc := NewMockDataClient(mockDB)
	opts := FetchCabTripsQuery{
		CabID:    testMedallionID,
		PickDate: testPickDate,
		NoCache:  false,
	}
	// First time cached should be false
	resp, cached, err := dc.FetchCabTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	// Second  time cached should be false
	fmt.Printf("Cached 1st time: %t \n", cached)

	resp, cached, err = dc.FetchCabTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	//time.Sleep(1 * time.Second)
	fmt.Printf("Cached 2nd time: %t \n", cached)
	if !cached {
		log.Println("Caching is not working, second time should be cached.")
		t.FailNow()
	}
	trips, ok := resp.([]CabTrip)
	if !ok {
		log.Println("Invalid response")
		t.FailNow()
	}
	if len(trips) != 1 {
		log.Println("Invalid number of trips")
		t.FailNow()
	}
	if trips[0].Medallion != testMedallionID {
		log.Println("Invalid medallion ID for results")
		t.FailNow()
	}
}

// TestMedallionSearchByDateOverrideCache will validate the cache override (NoCache flag) the second fetch should come from the DB and not from the cache.
func TestMedallionSearchByDateOverrideCache(t *testing.T) {
	testMedallionID := "D7D598CD99978BD012A87A76A7C891B7"
	testPickDate := "2013-12-01"
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionID, "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionID, testPickDate).WillReturnRows(rows)
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionID, testPickDate).WillReturnRows(rows)

	dc := NewMockDataClient(mockDB)
	opts := FetchCabTripsQuery{
		CabID:    testMedallionID,
		PickDate: testPickDate,
		NoCache:  true,
	}
	// First time cached should be false
	_, cached, err := dc.FetchCabTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	// Second  time cached should be true (because NoCache  is true)
	fmt.Printf("Cached 1st time: %t \n", cached)

	_, cached, err = dc.FetchCabTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	//time.Sleep(1 * time.Second)
	fmt.Printf("Cached 2nd time: %t \n", cached)
	if cached {
		log.Println("Cache override is not working, second time shouldn't be cached (NoCache is true)")
		t.FailNow()
	}

}

// TestMedallionMultiple validates the method FetchTrips can pick information from more than one medallion
func TestMedallionMultiple(t *testing.T) {
	testMedallionIDs := []string{"D7D598CD99978BD012A87A76A7C891B7", "A7D598CD99978BD012A87A76A7C891B9"}
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionIDs[0], "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1)).
		AddRow(testMedallionIDs[1], "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionIDs[1], testMedallionIDs[0]).WillReturnRows(rows)

	dc := NewMockDataClient(mockDB)
	opts := FetchTripsQuery{
		CabID:   testMedallionIDs,
		NoCache: false,
	}
	resp, _, err := dc.FetchTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	trips, ok := resp.([]CabTrip)
	if !ok {
		log.Println("Invalid response")
		t.FailNow()
	}
	if len(trips) != 2 {
		log.Println("Invalid number of trips")
		t.FailNow()
	}

}

// TestMedallionMultipleCached will call the method FetchTrip twice, the second call should come from the cache
func TestMedallionMultipleCached(t *testing.T) {
	testMedallionIDs := []string{"D7D598CD99978BD012A87A76A7C891B7", "A7D598CD99978BD012A87A76A7C891B9"}
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionIDs[0], "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1)).
		AddRow(testMedallionIDs[1], "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionIDs[1], testMedallionIDs[0]).WillReturnRows(rows)

	dc := NewMockDataClient(mockDB)
	opts := FetchTripsQuery{
		CabID:   testMedallionIDs,
		NoCache: false,
	}
	_, cached, err := dc.FetchTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	_, cached, err = dc.FetchTrips(opts)
	if err != nil {
		log.Printf("Invalid response: %v", err)
		t.FailNow()
	}
	if !cached {
		log.Printf("Cache is not working, second fetch should come from cache: %v", err)
		t.FailNow()
	}
}
