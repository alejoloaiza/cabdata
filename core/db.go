package core

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
)

// Real Mysql Database
func newMysqlDatabase(DatabaseOpts *DatabaseOptions) Database {
	sqlInfo := fmt.Sprintf("%s:%s@(%s:%d)/%s", DatabaseOpts.UserID, DatabaseOpts.Password, DatabaseOpts.Host, DatabaseOpts.Port, DatabaseOpts.Name)
	fmt.Println(sqlInfo)
	db, err := sqlx.Connect("mysql", sqlInfo)
	if err != nil {
		log.Printf("Error while connecting to SQL database: %+v", err)
	}
	return &MySQLDatabase{
		DB:  db,
		URL: sqlInfo,
	}

}

func (db *MySQLDatabase) Rebind(query string) string {
	// if err := db.Ping(); err != nil {
	// 	return ""
	// }
	return db.DB.Rebind(query)
}
func (db *MySQLDatabase) Select(dest interface{}, query string, args ...interface{}) error {
	err := db.Ping()
	if err != nil {
		//fmt.Printf("inside Ping Select %v", err)
		return err
	}
	return db.DB.Select(dest, query, args...)
}
func (db *MySQLDatabase) Exec(query string, args ...interface{}) (sql.Result, error) {
	if err := db.Ping(); err != nil {
		return nil, err
	}
	return db.DB.Exec(query, args...)
}
func (db *MySQLDatabase) Get(dest interface{}, query string, args ...interface{}) error {
	if err := db.Ping(); err != nil {
		return err
	}
	return db.DB.Get(dest, query, args...)
}

func (db *MySQLDatabase) Ping() error {
	if db.DB == nil || db.DB.Ping() != nil {
		newDB, err := sqlx.Connect("mysql", db.URL)
		if err != nil {
			log.Printf("Error while connecting to SQL database: %+v", err)
			return err
		}
		db.DB = newDB
	}
	return nil
}

// Mock database
func newMockMysql(mockDB *sql.DB) Database {

	db := sqlx.NewDb(mockDB, "mysql")
	return &MockDatabase{DB: db}
}

func (db *MockDatabase) Rebind(query string) string {
	return db.DB.Rebind(query)
}

func (db *MockDatabase) Select(dest interface{}, query string, args ...interface{}) error {
	return db.DB.Select(dest, query, args...)
}
func (db *MockDatabase) Exec(query string, args ...interface{}) (sql.Result, error) {
	return db.DB.Exec(query, args...)
}
func (db *MockDatabase) Get(dest interface{}, query string, args ...interface{}) error {
	return db.DB.Get(dest, query, args...)
}
