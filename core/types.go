package core

import (
	"database/sql"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type DataClient struct {
	Cache Cache
	DB    Database
}

type Cache interface {
	Set(string, interface{}) error
	Get(string) ([]byte, error)
	Exists(string) bool
	FlushAll() error
}

type Database interface {
	Select(interface{}, string, ...interface{}) error
	Exec(string, ...interface{}) (sql.Result, error)
	Rebind(string) string
	Get(interface{}, string, ...interface{}) error
}

type MySQLDatabase struct {
	DB  *sqlx.DB
	URL string
}

type RedisCache struct {
	Cache *redis.Client
}

type APIOptions struct {
	BaseURL string
	APIKey  string
}

type CacheOptions struct {
	Server   string
	Password string
}

type DatabaseOptions struct {
	Host     string
	Name     string
	Port     int
	UserID   string
	Password string
}

type FetchTripsQuery struct {
	CabID   []string
	NoCache bool
}

type FetchCabTripsQuery struct {
	CabID    string
	NoCache  bool
	PickDate string
}

type CabTrip struct {
	Medallion       string  `json:"medallion" db:"medallion"`
	HackLicense     string  `json:"hack_license" db:"hack_license"`
	VendorID        string  `json:"vendor_id" db:"vendor_id"`
	RateCode        int64   `json:"rate_code" db:"rate_code"`
	StoreAndFwdFlag string  `json:"store_and_fwd_flag" db:"store_and_fwd_flag"`
	PickDatetime    string  `json:"pickup_datetime" db:"pickup_datetime"`
	DropDatetime    string  `json:"dropoff_datetime" db:"dropoff_datetime"`
	PassengerCount  int64   `json:"passenger_count" db:"passenger_count"`
	TripTime        int64   `json:"trip_time_in_secs" db:"trip_time_in_secs"`
	TripDistance    float64 `json:"trip_distance" db:"trip_distance"`
}
type MockDatabase struct {
	DB *sqlx.DB
}
type MockCache struct {
	Cache *miniredis.Miniredis
}
