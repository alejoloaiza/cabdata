package server

import (
	"cabdata/core"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/gorilla/mux"
)

var m *mux.Router

var respRec *httptest.ResponseRecorder

// The purpose of this test file is testing the router and the handlers
func TestHandlerTrip(t *testing.T) {
	testMedallionID := "D7D598CD99978BD012A87A76A7C891B7"
	testPickDate := "2013-12-01"
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionID, "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionID, testPickDate).WillReturnRows(rows)

	dc := core.NewMockDataClient(mockDB)
	m = mux.NewRouter()
	addHandlers(dc, m)
	req, err := http.NewRequest("GET", fmt.Sprintf("/trip/%s?date=%s&nocache=true", testMedallionID, testPickDate), nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	m.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `[
 {
  "medallion": "D7D598CD99978BD012A87A76A7C891B7",
  "hack_license": "a",
  "vendor_id": "b",
  "rate_code": 1,
  "store_and_fwd_flag": "x",
  "pickup_datetime": "2013-12-01 00:13:00",
  "dropoff_datetime": "d",
  "passenger_count": 1,
  "trip_time_in_secs": 1,
  "trip_distance": 1
 }
]`
	equal, _ := AreEqualJSON(rr.Body.String(), expected)
	fmt.Println(equal)
	if !equal {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestHandlerTrips(t *testing.T) {
	testMedallionID := "D7D598CD99978BD012A87A76A7C891B7"
	mockDB, sqlmock, err := sqlmock.New()
	if err != nil {
		panic(err)
	}
	rows := sqlmock.NewRows([]string{"medallion", "hack_license", "vendor_id", "rate_code", "store_and_fwd_flag", "pickup_datetime", "dropoff_datetime", "passenger_count", "trip_time_in_secs", "trip_distance"}).
		AddRow(testMedallionID, "a", "b", int64(1), "x", "2013-12-01 00:13:00", "d", int64(1), float64(1), float64(1)).
		AddRow(testMedallionID, "e", "b", int64(1), "x", "2013-12-02 00:13:00", "d", int64(1), float64(1), float64(1))
	sqlmock.ExpectQuery("^SELECT (.+) FROM cab_trip_data WHERE (.+) ").WithArgs(testMedallionID).WillReturnRows(rows)

	dc := core.NewMockDataClient(mockDB)
	m = mux.NewRouter()
	addHandlers(dc, m)
	req, err := http.NewRequest("GET", fmt.Sprintf("/trips?medallion=%s&nocache=true", testMedallionID), nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	m.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `[
 {
  "medallion": "D7D598CD99978BD012A87A76A7C891B7",
  "hack_license": "a",
  "vendor_id": "b",
  "rate_code": 1,
  "store_and_fwd_flag": "x",
  "pickup_datetime": "2013-12-01 00:13:00",
  "dropoff_datetime": "d",
  "passenger_count": 1,
  "trip_time_in_secs": 1,
  "trip_distance": 1
 },
 {
	"medallion": "D7D598CD99978BD012A87A76A7C891B7",
	"hack_license": "e",
	"vendor_id": "b",
	"rate_code": 1,
	"store_and_fwd_flag": "x",
	"pickup_datetime": "2013-12-02 00:13:00",
	"dropoff_datetime": "d",
	"passenger_count": 1,
	"trip_time_in_secs": 1,
	"trip_distance": 1
   }
]`
	equal, _ := AreEqualJSON(rr.Body.String(), expected)
	fmt.Println(equal)
	if !equal {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestHandlerDelete(t *testing.T) {
	mockDB, _, err := sqlmock.New()
	if err != nil {
		panic(err)
	}

	dc := core.NewMockDataClient(mockDB)
	m = mux.NewRouter()
	addHandlers(dc, m)
	req, err := http.NewRequest("DELETE", "/cache", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	m.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}

func AreEqualJSON(s1, s2 string) (bool, error) {
	var o1 interface{}
	var o2 interface{}

	var err error
	err = json.Unmarshal([]byte(s1), &o1)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 1 :: %s", err.Error())
	}
	err = json.Unmarshal([]byte(s2), &o2)
	if err != nil {
		return false, fmt.Errorf("Error mashalling string 2 :: %s", err.Error())
	}

	return reflect.DeepEqual(o1, o2), nil
}
