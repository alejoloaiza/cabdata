package server

import (
	"cabdata/core"
	"net/http"
)

type ServerOptions struct {
	Address string
}

type clientHandler struct {
	client  *core.DataClient
	handler func(c *core.DataClient, w http.ResponseWriter, r *http.Request) (interface{}, *HandlerError)
}

type HandlerError struct {
	Error   error
	Message string
	Code    int
}

type Handler func(w http.ResponseWriter, r *http.Request) (interface{}, *HandlerError)
