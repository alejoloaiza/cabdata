package server

import (
	"bytes"
	"cabdata/core"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func Start(c *core.DataClient, opts *ServerOptions) {

	log.Printf("RUNNING SERVER [%s]", opts.Address)

	r := mux.NewRouter()

	crs := cors.AllowAll()
	addHandlers(c, r)
	handler := crs.Handler(r)

	log.Fatal(http.ListenAndServe(opts.Address, handler))

}

func addHandlers(c *core.DataClient, r *mux.Router) {
	// This endpoint will give trips by one or more cabs (medallion)
	r.Handle("/trips", client(c, getTrips)).Methods("GET")
	// This endpoint will provide the trips for one medallion and for a given date.
	r.Handle("/trip/{medallion}", client(c, getCabTrips)).Methods("GET")
	//There must be also be a method to clear the cache.
	r.Handle("/cache", client(c, deleteCache)).Methods("DELETE")

}

func client(client *core.DataClient, handler func(c *core.DataClient, w http.ResponseWriter, r *http.Request) (interface{}, *HandlerError)) clientHandler {
	return clientHandler{
		client:  client,
		handler: handler,
	}
}

func (h clientHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	initTime := time.Now().UnixNano()
	response, err := h.handler(h.client, w, r)

	writeEscapedResponse(initTime, w, r, response, err)
}

func writeEscapedResponse(init int64, w http.ResponseWriter, r *http.Request, response interface{}, err *HandlerError) {
	if err != nil {
		log.Printf("%s %s %s %d ERROR: %s \n", r.RemoteAddr, r.Method, r.URL, err.Code, err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s"}`, err.Message), err.Code)
		return
	}

	var e error

	buf := new(bytes.Buffer)

	if response != nil {
		if bResp, ok := response.([]byte); ok {
			w.Write(bResp)
		} else {
			enc := json.NewEncoder(buf)
			enc.SetEscapeHTML(true)
			enc.SetIndent("", " ")
			e = enc.Encode(response)
			if e != nil {
				http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
				return
			}
			w.Write(buf.Bytes())
		}
	}
	end := time.Now().UnixNano()

	log.Printf("%s %s %s %d %d ms", r.RemoteAddr, r.Method, r.URL, 200, ((end - init) / int64(time.Millisecond)))
}

func Error(err error, code int) *HandlerError {
	return &HandlerError{
		Error:   err,
		Message: err.Error(),
		Code:    code,
	}
}

func InternalError(err error) *HandlerError {
	return &HandlerError{
		Error:   err,
		Message: err.Error(),
		Code:    http.StatusInternalServerError,
	}
}

func BadRequest(err error) *HandlerError {
	return &HandlerError{
		Error:   err,
		Message: err.Error(),
		Code:    http.StatusBadRequest,
	}
}

func ExtractDefault(q url.Values, property string, def []string) []string {
	val := q[property]
	if len(val) == 0 {
		return def
	}
	return val
}

func Extract(q url.Values, property string) []string {
	return ExtractDefault(q, property, nil)
}

func ExtractLower(q url.Values, property string) []string {
	return AsLowercase(Extract(q, property))
}

func ExtractFirstLower(q url.Values, property string) string {
	r := ExtractLower(q, property)
	if len(r) > 0 {
		return r[0]
	}
	return ""
}

func AsLowercase(array []string) []string {
	for i, s := range array {
		array[i] = strings.ToLower(s)
	}
	return array
}
func Get(r *http.Request, name string) string {
	return mux.Vars(r)[name]
}
