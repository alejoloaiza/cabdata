package server

import (
	"cabdata/core"
	"errors"
	"net/http"
	"time"
)

const (
	dateLayout = "2006-02-01"
)

func getTrips(c *core.DataClient, w http.ResponseWriter, r *http.Request) (interface{}, *HandlerError) {
	q := r.URL.Query()
	cabID := Extract(q, "medallion")
	nocache := ExtractFirstLower(q, "nocache")
	if len(cabID) < 1 {
		return nil, BadRequest(errors.New("provide at least one medallion for the search"))
	}
	opts := core.FetchTripsQuery{CabID: cabID, NoCache: nocache == "true"}

	value, _, err := c.FetchTrips(opts)
	if err != nil {
		return nil, InternalError(err)
	}
	return value, nil
}

func getCabTrips(c *core.DataClient, w http.ResponseWriter, r *http.Request) (interface{}, *HandlerError) {
	q := r.URL.Query()
	cabID := Get(r, "medallion")
	date := ExtractFirstLower(q, "date")
	nocache := ExtractFirstLower(q, "nocache")
	if date != "" {
		_, err := time.Parse(dateLayout, date)
		if err != nil {
			return nil, BadRequest(errors.New("error with the date format remember to use YYYY-MM-DD"))
		}
	}
	//fmt.Println(cabID, date)
	opts := core.FetchCabTripsQuery{CabID: cabID, PickDate: date, NoCache: nocache == "true"}

	value, _, err := c.FetchCabTrips(opts)
	if err != nil {
		return nil, InternalError(err)
	}
	return value, nil
}

func deleteCache(c *core.DataClient, w http.ResponseWriter, r *http.Request) (interface{}, *HandlerError) {

	value, err := c.DeleteCache()
	if err != nil {
		return nil, InternalError(err)
	}
	return value, nil
}
