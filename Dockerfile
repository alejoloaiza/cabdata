FROM golang

WORKDIR /go/src/cabdata
COPY . .

RUN go build -o cabdata
EXPOSE 5000
CMD ["./cabdata"]