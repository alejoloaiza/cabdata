#!/bin/bash
export REDIS_SERVER=127.0.0.1:6379
export SERVER_PORT=:5000
export DB_HOST=127.0.0.1
export DB_PORT=3306
export DB_USER=root
export DB_NAME=cabdata
export DB_PASSWORD=my-secret-pw
export RUNAS=server