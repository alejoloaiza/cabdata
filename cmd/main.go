package cmd

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

const defaultURL = "http://localhost:5000"

func Start() {
	baseURL := os.Getenv("SERVER_URL")
	if baseURL == "" {
		baseURL = defaultURL
	}
	caller := newAPICaller(baseURL)
	var cabID string
	var pickDate string
	var noCache bool
	var cmdFetchData = &cobra.Command{
		Use:   "fetch [medallion] [pick date YYYY-MM-DD] [Ignore Cache true/false]",
		Short: "Get cab trips by date",
		Long:  `Fetch trips for a given cab on a particular date`,
		Args:  cobra.MinimumNArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			url := fmt.Sprintf("trip/%s?date=%s&nocache=%s", args[0], args[1], args[2])
			fmt.Println(url)
			res, err := caller.makeRequest("GET", url, nil)
			if err != nil {
				log.Printf("error with http request: %v", err)
				return
			}
			printResponse(res)
		},
	}
	cmdFetchData.Flags().StringVarP(&cabID, "medallion", "m", "", "identification of the cab")
	cmdFetchData.Flags().StringVarP(&pickDate, "date", "d", "", "pickup date in the format YYYY-MM-DD")
	cmdFetchData.Flags().BoolVarP(&noCache, "cache", "c", false, "ignore cache? true/false")

	var cmdFetchMultipleData = &cobra.Command{
		Use:   "fetchmultiple [medallion1,medallion2,...,medallionN] [Ignore Cache true/false]",
		Short: "Get multiple cab trips",
		Long:  `Fetch trips for multiple cabs `,
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			var medallion string
			for _, m := range strings.Split(args[0], ",") {
				medallion += fmt.Sprintf("medallion=%s&", m)
			}
			url := fmt.Sprintf("trips?%s&nocache=%s", medallion, args[1])
			fmt.Println(url)
			res, err := caller.makeRequest("GET", url, nil)
			if err != nil {
				log.Printf("error with http request: %v", err)
				return
			}
			printResponse(res)
		},
	}
	cmdFetchMultipleData.Flags().StringVarP(&cabID, "medallion", "m", "", "identification of the cabs")
	cmdFetchMultipleData.Flags().BoolVarP(&noCache, "cache", "c", false, "ignore cache? true/false")

	var cmdClear = &cobra.Command{
		Use:   "clear",
		Short: "clears the cache",
		Long:  `delete all keys from cache`,
		Args:  cobra.MinimumNArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			res, err := caller.makeRequest("DELETE", "cache", nil)
			if err != nil {
				log.Printf("error with http request: %v", err)
				return
			}
			printResponse(res)
		},
	}
	var rootCmd = &cobra.Command{Use: "cabdata"}
	rootCmd.AddCommand(cmdFetchData, cmdFetchMultipleData, cmdClear)
	rootCmd.Execute()

}

func newAPICaller(baseURL string) *APICaller {
	return &APICaller{
		baseURL:    baseURL,
		httpClient: DefaultClient(),
	}
}

type APICaller struct {
	httpClient *http.Client
	baseURL    string
}

func (a *APICaller) makeRequest(method, endpoint string, body io.Reader) (*http.Response, error) {

	url := a.baseURL + "/" + endpoint
	//fmt.Println(url)
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	resp, err := a.httpClient.Do(req)
	req.Close = true

	return resp, err
}

func printResponse(r *http.Response) {
	defer r.Body.Close()
	resp, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error reading response: %v", err)
	}
	fmt.Printf("%s", string(resp))
}
